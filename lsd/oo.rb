require "matrix"

# TODO:
# * Characterize problems with smallest basins of attraction for global min.
# * " " " costliest local min. relative to global min.

class Qap
  attr_accessor :n, :points, :dist, :floor, :costs, :global_minimum, :sf

  def initialize(points, flow)
    @n = points.count
    @dist = Matrix.zero(@n, @n)
    ut_indices.each do |i, j|
      d = (points[i]-points[j]).norm
      @dist[i, j] = d
      @dist[j, i] = d
    end
    @points = points
    @flow = flow
  end

  def self.foo_goo_experiment(n = 6, t = 60) # this experiment was a bust
    t0 = Time.new.to_f
    table = []
    while Time.new.to_f-t0 < t
      q = Qap.random(n)
      table << [
        # q.foo_factor,
        # q.goo_factor,
        q.max_cost_penalty,
        q.suboptimality_probability,
        q.vod]
    end
    file_name = "#{Time.new.to_i}-#{n}.csv"
    File.open(file_name, File::RDWR|File::CREAT, 0644) do |f|
      f.write("max_cost_penalty, subopt_prob, vod\n")
      f.write(table.map {|line| line.join(", ")}.join("\n"))
    end
    file_name
  end

  # measurable parameters of a problem:

  def max_cost_penalty # max cost penalty of going with a local minimum
    k = local_minima!.values
    k.max-k.min
  end

  def suboptimality_probability # proportion of perms w. suboptimal results
    go!
    Float(steepest_and_flattest!.count {|_, p, _| p != @global_minimum})/@costs.count
  end

  def noncolinearity
    @points.combination(3).map do |a, b, c|
      Matrix.columns([a,b,c]).vstack(Matrix[[1,1,1]]).det.abs
    end.sum
  end

  def vod # variance of distances
    avg = @dist.entries.sum/@dist.entries.length
    @dist.entries.map {|x| (x-avg)**2}.sum
  end

  def self.easy_cases(n = 6, t = 600)
    t0 = Time.new.to_f
    collection = []
    trials = 0
    while Time.new.to_f-t0 < t and collection.length < 20
      q = Qap.random(n)
      trials += 1
      if q.suboptimality_probability.zero?
        collection << q
        puts "#{collection.length} found so far, after #{trials} tried"
      end
    end
    File.open("#{Time.new.to_i}.html", File::RDWR|File::CREAT, 0644) do |html|
      html.write(%Q(
        <!DOCTYPE HTML>
        <html lang="en">
        <head>
          <meta charset="utf-8 /">
          <title>Easy cases</title>
          <style>
            figure {
              float: left;
            }
          </style>
        </head>
        <body>
          <h1>An assortment of QAPs for which steepest ascent always leads to global minimum</h1>
          <h2>#{trials} trials<br />
          #{collection.length} easy cases found</h2>
      ))
      collection.sample(20).each_with_index do |q, i|
        file_name = "#{i}.svg"
        File.open(file_name, "w") do |f|
          f.write(q.draw(q.global_minimum))
        end
        html.write(%Q(
          <figure>
            <img src="#{file_name}" />
          </figure>
        ))
      end
      html.write(%Q(
        </body>
        </html>
      ))
    end
  end

  def self.awfulness_hull(n = 6, t = 600)
    t0 = Time.new.to_f
    collection = []
    trials = 0
    while Time.new.to_f-t0 < t
      q = Qap.random(n)
      trials += 1
      mcp = q.max_cost_penalty
      sp = q.suboptimality_probability
      collection.delete_if do |_, m, s|
        m <= mcp && s <= sp
      end
      collection << [q, mcp, sp] if collection.none? do |_, m, s|
        m >= mcp && s >= sp
      end
    end
    File.open("#{Time.new.to_i}.html", File::RDWR|File::CREAT, 0644) do |html|
      html.write(%Q(
        <!DOCTYPE HTML>
        <html lang="en">
        <head>
          <meta charset="utf-8 /">
          <title>Awfulness hull for #{t} seconds of #{n}-node problems</title>
          <style>
            figure {
              float: left;
            }
          </style>
        </head>
        <body>
          <h1>Awfulness hull for #{t} seconds of #{n}-node problems</h1>
          <h2>#{trials} trials</h2>
      ))
      collection.sort_by {|_, m, _| m}.each_with_index do |(q, m, s), i|
        file_name = "#{i}.svg"
        File.open(file_name, "w") do |f|
          f.write(q.draw(q.global_minimum))
        end
        puts "m=#{m}, s=#{s}"
        html.write(%Q(
          <figure>
            <figcaption>m=#{m}, s=#{s}</figcaption>
            <img src="#{file_name}" />
          </figure>
        ))
      end
      html.write(%Q(
        </body>
        </html>
      ))
    end
    nil # TODO: don't return nil
  end

  def foo_factor # no particular reason
    require_relative "../qap2.rb" # TODO: neaten up directories
    @dist.entries.sort.zip(@flow.entries.sort).corr
  end

  def goo_factor
    require_relative "../qap2.rb" # TODO: neaten up directories
    @dist.row_vectors.map(&:sum).sort.zip(@flow.row_vectors.map(&:sum).sort).corr
  end

  def self.random(n)
    points = Array.new(n) {Vector[rand, rand]}
    flow = Matrix.build(n, n) {rand(0..2)}
    new(points, flow)
  end

  def cost(perm)
    return @costs[perm] if @costs
    ut_indices.map do |i, j|
      @dist[i, j]*@flow[perm[i], perm[j]]
    end.sum
  end

  def cost_hash!
    @costs = (0...@n).to_a.permutation.map {|p| [p, cost(p)]}.to_h
  end

  def local_minima!
    return @local_minima if @local_minima
    cost_hash! unless @costs
    @local_minima = @costs.select do |perm, cost|
      transpositions(perm).none? do |p|
        cost(p) < cost
      end
    end
  end

  def steepest_descent(perm)
    p1 = perm
    p1_cost = cost(p1)
    loop do
      p, c = transpositions(p1).map {|p| [p, cost(p)]}.min_by {|p, c| c}
      return {p1 => p1_cost} if c >= p1_cost
      p1 = p
      p1_cost = c
    end
  end

  def flattest_descent(perm)
    p1 = perm
    p1_cost = cost(p1)
    loop do
      descents = transpositions(p1).map {|p| [p, cost(p)]}.select do |_, c|
        c < p1_cost
      end
      return {p1 => p1_cost} if descents.empty?
      p, c = descents.max_by {|_, c| c}
      return {p1 => p1_cost} if c >= p1_cost
      p1 = p
      p1_cost = c
    end
  end

  def steepest_and_flattest!
    return @sf if @sf
    cost_hash! unless @costs
    @sf = @costs.map do |p, _|
      sdc = steepest_descent(p).keys[0]
      fdc = flattest_descent(p).keys[0]
      [p, sdc, fdc]
    end
  end

  def self.steepest_vs_flattest(n = 6, t = 600)
    t0 = Time.new.to_i
    s = 0
    f = 0
    c = 0
    while Time.new.to_i-t0 < t
      q = Qap.random(n)
      q.go!
      q.steepest_and_flattest!
      ss = q.sf.count {|_, p, _| p == q.global_minimum}
      ff = q.sf.count {|_, _, p| p == q.global_minimum}
      s += 1 if ss > ff
      f += 1 if ff > ss
      c += 1 if ff == ss
      puts "#{[s, c, f]}"
    end
    [s, c, f]
  end

  def path(start, rate = :steepest, dir = :descent)
    raise DomainError unless [:steepest, :flattest].include?(rate)
    raise DomainError unless [:descent, :ascent].include?(dir)
    result = [start]
    loop do
      rl = result.last
      cl = cost(result.last)
      t = transpositions(result.last)
      t.map! {|x| [x, cost(x)]}
      t.select! {|_, c| dir == :descent ? c < cl : c > cl}
      return result if t.empty?
      if (rate == :steepest) == (dir == :descent)
        result << t.min_by {|_, c| c}[0]
      else
        result << t.max_by {|_, c| c}[0]
      end
    end
  end

  def descent_graph!
    return @descent_graph if @descent_graph
    @descent_graph = []
    cost_hash!
    @costs.each do |p, c|
      next if @descent_graph.flatten(1).include?(p)
      cp = @costs[p]
      transpositions(p).each do |x|
        @descent_graph << [p, x] if @costs[p] >= @costs[x]
        @descent_graph << [x, p] if @costs[p] <= @costs[x]
      end
    end
    @descent_graph
  end

  def go!
    return @global_minimum if @global_minimum
    cost_hash! unless @costs
    @global_minimum = @costs.min_by {|_, c| c}[0]
  end

  def jumps_from_steepest!
    steepest_and_flattest!
    go!
    go_basin = @sf.select {|_, s, _| s == @global_minimum}.map(&:first)
    lm = local_minima!.keys-[@global_minimum]
    lm.map do |p|
      # go_basin.min_by do |w|
      go_basin.map do |w|
        w.zip(p).count {|i, j| i != j}
      end.min
    end.zip(lm)
  end

  # TODO: This is erroneous, needs debugging.
  def watershed(perm) # based on steepest descent, maybe generalize later
    steepest_and_flattest! unless @sf
    if local_minima!.include?(perm)
      @sf.select {|_, s, _| s == perm}.map(&:first)
    else
      cand = @sf.select do |_, s, _|
        steepest_descent(perm).keys.include?(s)
      end.map(&:first)
      puts "#{cand.count} candidates"
      cand.select do |p|
        path = [p]
        while path.last != perm
          pl = path.last
          c = transpositions(path.last).select {|t| @costs[pl] > @costs[t]}
          break if c.empty?
          path << c.min_by do |t|
            @costs[t]
          end
        end
        path.last == p
      end
    end
  end

  def draw(perm)
    svg = %Q(
      <svg version="1.1" baseProfile="full" width="300" height="300"
        viewBox="-0.1 -0.1 1.2 1.2" xmlns="http://www.w3.org/2000/svg">
      <rect x="0" y="0" height="1" width="1" fill="green" />
    )
    n = @points.count
    svg << (0...n).to_a.combination(2).map do |n1, n2|
      i, j = [n1, n2].sort
      f = @flow[perm[i], perm[j]]
      next if f.zero?
      "<line x1=\"#{@points[i][0]}\" x2=\"#{@points[j][0]}\" y1=\"#{@points[i][1]}\" y2=\"#{@points[j][1]}\" stroke=\"orange\" stroke-width=\"#{f*0.005}\" />"
    end.join("\n")
=begin
    svg << @points.zip(0..).map do |point, index|
      x, y = point.to_a
      node = "<circle cx=\"#{x}\" cy=\"#{y}\" r=\"0.05\" fill=\"yellow\" />\n"
      node << "<text x=\"#{x}\" y=\"#{y}\" font-size=\"0.07px\""
      node << " dominant-baseline=\"middle\" text-anchor=\"middle\">#{index}</text>\n"
      node
    end.join
=end
    svg << %Q(
      </svg>
    )
  end

  private

  def ut_indices
    (0...@n).to_a.combination(2)
  end

  def transpositions(perm)
    ut_indices.map do |i, j|
      p = perm.clone
      p[i] = perm[j]
      p[j] = perm[i]
      p
    end
  end
end
