# least steep descent
#
#

# generate random QAP problem
def random_problem(n = 9)
  pts = random_points(n)
  dist = dist_mat(pts)
  flow = random_flow(n)
  [pts, dist, flow]
end

def transpositions(perm)
  n = perm.count
  (0...n).to_a.combination(2).map do |i, j|
    p = perm.clone
    p[i] = perm[j]
    p[j] = perm[i]
  end
end

def lsd(pts, dist, flow, perm)
  transpositions(perm).sort_by {|p| cost(dist, flow, p)}
end

# Calculate the cost of a given ordering of the points.
# Default permutation is (0...n).
def cost(dist, flow, perm = (0...flow.row_count).to_a)
  n = flow.row_count
  score = 0
  (0...n).each do |i|
    (i...n).each do |j|
      score += flow[perm[i], perm[j]]*dist[i, j]
    end
  end
  score
end


