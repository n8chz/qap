require "matrix"

def dist(p1, p2)
  x1, x2 = p1
  y1, y2 = p2
  Math::sqrt((x1-y1)**2+(x2-y2)**2)
end

class Qap
  attr_accessor :flow, :locations

  def initialize(n = 10)
    @flow = Matrix.build(n, n) {rand(-2..2)}
    @size = @flow.row_count
    n = Math::sqrt(@size).ceil
    @points = (0...n).to_a.product((0...n).to_a)
    pairs = @points.combination(2)
    @distances = pairs.map do |p1, p2|
      [[p1, p2], dist(p1, p2)]
    end.to_h
    random_layout!
  end

  def random_layout!
    @locations = @points.sample(@size)
    self
  end

  def display_layout
    n = Math::sqrt(@size).ceil
    Matrix.build(n, n) do |i, j|
      (@locations.index([i, j]) || -1)+1
    end
  end

  def optimize!(seconds = 60)
    min_e = evaluate_layout
    min_layout = @locations.clone
    n = 0
    start = Time.now().to_i
    loop do
      n += 1
      random_layout!
      e = evaluate_layout
      if e < min_e
        min_e = e
        min_layout = @locations.clone
        elapsed = Time.now.to_i-start
        puts "#{'%6d'%elapsed} #{'%5.7f'%e}"
      end
      if Time.now().to_i-start > seconds
        @locations = min_layout
        puts "#{n} layouts tested"
        return min_e
      end
    end
  end

  def evaluate_layout
    (0...@size).to_a.combination(2).map do |pair|
      i, j = pair.sort
      locations = [@locations[i], @locations[j]].sort
      #todo: the following doesn't evaluate:
      @distances[locations]*@flow[i, j]
    end.sum
  end
end
