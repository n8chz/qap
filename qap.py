import numpy as np
from itertools import permutations, product, combinations
from math import inf
import random
import graphviz
from time import time, gmtime, strftime

def nxnxnxn(n):
    return product(range(n), range(n), range(n), range(n))


class Qap:
    def __init__(self, a, b):
        try:
            m1, self.n = a.shape
            m2, n2 = b.shape
            if len(set([m1, self.n, m2, n2])) != 1:
                raise Exception("Matrices must be same shape")
        except:
            raise Exception("a and b parameters must be square matrices")
        self.a = a
        self.b = b
        self.table = np.zeros((self.n, self.n, self.n, self.n), dtype=float)
        for i1 in range(self.n):
            for j1 in range(self.n):
                for i2 in range(self.n):
                    for j2 in range(self.n):
                        self.table[i1, j1, i2, j2] = self.a[i1, j1]*self.b[i2, j2]
        # self.unsolve()
        self.min_cost = inf


    # Cost of assignment p.
    # Assigning facility f to location L is denoted by p[L]==f.
    # p can be a partial assignment. If L is an unassigned location,
    # then p[L]==None. If f is an unassigned facility, then
    # f not in p.values().
    def cost(self, p=None):
        p = p or self.p
        z = 0
        for i in range(self.n):
            if p[i] != None:
                for j in range(i):
                    if p[j] != None:
                        z += self.table[p[i], p[j], i, j]
        if z < self.min_cost:
            self.min_cost = z
            self.optimum = p
        return z

    # see Angel, E., & Zissimopoulos, V. (1998). On the quality of local search
    # for the quadratic assignment problem. Discrete applied mathematics,
    # 82(1-3), p. 16.
    def avg_cost(self):
        return sum(sum(self.a))*sum(sum(self.b))/self.n/(self.n-1)/2
                

    # A directed graph.
    # Vertices of the descent graph are permutations of the locations.
    # There is an edge from p1 to p2 iff
    # 1. permutations p1 and p2 differ by one swap (2-cycle)
    # 2. self.cost(p2) <= self.cost(p1)
    # The graph is implemented as a set of edge.
    # An edge is implemented as a tuple of permutations (vertices)
    def get_descent_graph(self):
        self.min_cost = inf
        self.costs = dict()
        self.descent_graph = set()
        steepest_descent = dict()
        perms =  permutations(tuple(range(self.n))) # DRY?
        for p in perms:
            cp = self.cost(p)
            self.costs[p] = cp
            if cp < self.min_cost:
                self.min_cost = cp
                self.optimum = p
            for i, j in combinations(range(self.n), 2):
                if p[j] < p[i]:
                    n = list(p)
                    n[i] = p[j]
                    n[j] = p[i]
                    n = tuple(n)
                    cn = self.costs[n]
                    if cp < cn:
                        self.descent_graph.add((n, p))
                    else:
                        self.descent_graph.add((p, n))
        return self.descent_graph

    def graphviz(self):
        try:
            self.descent_graph
        except:
            self.get_descent_graph()
        finally:
            dot = graphviz.Digraph()
            for node in self.costs.keys():
                dot.node(''.join(map(str, node)))
            for n1, n2 in self.descent_graph:
                n1 = ''.join(map(str, n1))
                n2 = ''.join(map(str, n2))
                dot.edge(n1, n2)
            file_name = strftime("%Y%m%d%H%M%S.gv", gmtime())
            dot.save(file_name)
            dot.render(file_name, format="svg").replace('\\', '/')


    # The following methods are for testing out various hypotheses
    # about the solution set.

    def opinionatedness(self):
        s = sum(sum(self.a[i, j] for j in range(i)) for i in range(self.n))
        m = 2*s/self.n/(self.n-1)
        a = (self.a-m)**2
        # a = abs(self.a-m)
        scores = zip(range(self.n), sum(a))
        return list(map(lambda x: x[0], sorted(scores, key=lambda x: -x[1])))

    def partial_assignment(self, nodes):
        z_min = inf
        for locations in permutations(range(self.n), len(nodes)):
            self.p = [None]*self.n
            for loc, node in zip(locations, nodes):
                self.p[loc] = node
            z = self.cost()
            if z < z_min:
                z_min = z
                loc_min = locations
        indices = set(range(self.n))
        self.assigned_nodes = set(nodes)
        self.assigned_locs = set(loc_min)
        self.free_nodes = indices-self.assigned_nodes
        self.free_locs = indices-self.assigned_locs
        self.p = [None]*self.n
        for loc, nod in zip(loc_min, nodes):
            self.p[loc] = nod
        return self.p

    # Choose an assignment permutation by first assigning the three most
    # "opinionated" facilities to whatever three locations are least
    # costly for them. Assign remaining facilities to their least costly
    # location (given previous assignments) in order by decreasing
    # "opinionatedness." Results in a better assignment than the mean over
    # all permutations about 2/3 of the time.
    def satisfice(self, i=3):
        nodes = self.opinionatedness()
        self.partial_assignment(nodes[:i])
        for node in nodes[i:]:
            self.assign_node(node)
        z_s = self.cost()
        return [z_s, self.p]


    def assign_first_three(self):
        assignment = [None]*self.n
        indices = list(range(self.n))
        ranking = self.opinionatedness()
        triad = ranking[0:3]
        # nodes = ranking[3:]
        return self.partial_assignment(triad)

    def assign_node(self, node):
        opt = inf
        for loc in self.free_locs:
            self.p[loc] = node
            z = self.cost()
            if z < opt:
                opt = z
                opt_loc = loc
            self.p[loc] = None
        self.p[opt_loc] = node
        self.assigned_nodes.add(node)
        self.assigned_locs.add(opt_loc)
        self.free_nodes.remove(node)
        self.free_locs.remove(opt_loc)
        return self.p

    def descent_step(self, p):
        # self.p = perm
        min_cost = orig_cost = self.cost(p)
        p = list(p)
        for i, j in combinations(range(self.n), 2):
            dummy = p[i]
            p[i] = p[j]
            p[j] = dummy
            c = self.cost(p)
            if c < min_cost:
                min_cost = c
                best_p = p.copy()
        if min_cost < orig_cost:
            return tuple(best_p)
        else:
            return None

    def steepest_descent(self, p):
        while True:
            q = self.descent_step(p)
            if q:
                p = q
            else:
                return p

    # Each permutation's steepest descent is (at most) one other permutation.
    # No permutation in the case of a local minimum.
    # This graph is represented as a dict.
    # steepest_descent[p1] is p2 iff
    # 1. p2 has lower cost than p1
    # 2. p2 has lowest cost of p1's 2-cycle neighbors




                

            


def neighbors(perm):
    p = list(perm)
    nn = []
    for i, j in combinations(perm, 2):
        n = p.copy()
        n[i] = p[j]
        n[j] = p[i]
        if n < perm:
            nn.append(tuple(n))
    return nn



def test_partial_assignment(n=6):
    for _ in range(10):
        q = random_qap(n)
        triple = random.sample(range(n), 3)
        x = q.partial_assignment(triple)
        print('{:s} {:s}'.format(str(triple), str(x)))
        # TODO: establish value of self.p



def random_symmetric(n=6):
    a = np.random.rand(n, n)
    for i in range(n):
        a[i, i] = 0
        for j in range(i):
            a[i, j] = a[j, i]
    return a




def random_distance_matrix(n=6):
    points = np.random.rand(n, 2)
    b = np.zeros((n, n))
    for i in range(n):
        for j in range(i):
            b[i, j] = np.linalg.norm(points[i]-points[j])
            b[j, i] = b[i, j]
    return b

def random_qap(n=6):
    a = random_symmetric(n)
    b = random_distance_matrix(n)
    return Qap(a, b)

# What is the probability that assigning the three most opinionated assignees
# first has better than average cost?
def question1(n=6):
    n_better = 0
    n_total = 0
    for i in range(100):
        q = random_qap(n)
        a = q.avg_cost()
        c, _ = q.satisfice()
        n_total += 1
        if c < a:
            n_better += 1
    return n_better/n_total

# What is the probability that opinionated-first assignment produces a better
# starting point than random for steepest descent?
def question2(n=6):
    n_better = 0
    n_total = 0
    for i in range(1000):
        q = random_qap(n)
        p = list(range(n))
        random.shuffle(p)
        q.steepest_descent(p)
        c_rand = q.cost(p)
        q.satisfice()
        q.steepest_descent(q.p)
        c_op = q.cost()
        n_total += 1
        if c_op < c_rand:
            n_better += 1
        elif c_op == c_rand:
            n_better += 0.5
    return n_better/n_total

