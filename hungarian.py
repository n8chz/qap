import random
import numpy as np
from itertools import combinations, permutations
from time import time
import math

def cover(pairs, lines = set()):
    pairs = [(i, j) for i, j in pairs if i not in lines and j not in lines]
    if pairs == []:
        return lines
    i, j = pairs[0]
    tail = pairs[1:]
    first = cover(tail, lines.union({i}))
    second = cover(tail, lines.union({j}))
    if len(first) < len(second):
        return first
    else:
        return second

def random_pairs(n=5):
    pair_set = {(i, np.random.randint(n, 2*n)) for i in range(n)}
    return list(pair_set.union({(np.random.randint(0, n), j) for j in
                                range(n)}))

def random_square_matrix(n=5):
    return np.random.randint(0, 100, size=(n, n))

def initial_zeros(matrix):
    n = matrix.shape[0]
    zeros = set()
    for i in range(n):
        smallest = min(matrix[i, ...])
        for j in range(n, 2*n):
            matrix[i, j-n] -= smallest
            if matrix[i, j-n] == 0:
                zeros.add((i, j))
    for j in range(n, 2*n):
        smallest = min(matrix[..., j-n])
        for i in range(n):
            matrix[i, j-n] -= smallest
            if matrix[i, j-n] == 0:
                zeros.add((i, j))
    return zeros

def more_zeros(matrix, lines):
    n = matrix.shape[0]
    rows = set(range(n))
    cols = set(range(n, 2*n))
    nonrows = rows-lines
    noncols = cols-lines
    zeros = set()
    smallest = min(min(matrix[i, j-n] for j in noncols) for i in nonrows)
    for i in rows.intersection(lines):
        for j in cols.intersection(lines):
            matrix[i, j-n] += smallest
    for i in nonrows:
        for j in noncols:
            matrix[i, j-n] -= smallest
    for i in rows:
        for j in cols:
            if matrix[i, j-n] == 0:
                zeros.add((i, j))
    return zeros


def assign(pairs):
    assigned = {}
    if len(pairs) == 0:
        return assigned
    n = max(i for i, _ in pairs)+1
    singletons = [k for k in range(2*n) if sum(1 for p in pairs if k in p) ==
                  1]
    for i, j in pairs:
        if i in singletons or j in singletons:
            assigned[i] = j
    survivors = {(i, j) for i, j in pairs if i not in assigned and j not in assigned.values()}
    k = n-len(assigned)
    for p in combinations(survivors, k):
        rows = {i for i, _ in p}
        cols = {j for _, j in p}
        if len(rows) == k and len(cols) == k:
            for i, j in p:
                assigned[i] = j
            return assigned
    raise "wtf"

def solve(matrix):
    n = matrix.shape[0]
    zeros = initial_zeros(matrix)
    while True:
        lines = cover(zeros)
        if len(lines) == n:
            return assign(zeros)
        zeros = more_zeros(matrix, lines)

def brute_force(matrix):
    n = matrix.shape[0]
    rows = tuple(range(n))
    min_cost = math.inf
    for p in permutations(range(n, 2*n)):
        s = dict(zip(rows, p))
        c = cost(matrix, s)
        if c < min_cost:
            min_cost = c
            best = s
    return best




def cost(matrix, soln):
    n = matrix.shape[0]
    return sum(matrix[i, soln[i]-n] for i in soln)

def test(n=5, t=60):
    # TODO: write the test!
    c = 0
    start = time()
    while time()-start < t:
        m = random_square_matrix(n)
        s = solve(m)
        b = brute_force(m)
        if cost(m, s) != cost(m, b):
            raise "fail after #{:d} successes".format(c)
        c += 1
    print('{:d} successful iterations in #{:.2f} seconds'.format(c, t))




