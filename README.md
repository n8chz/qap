## Hungarian algorithm in Ruby ##

Defines a class called `Assignment` which implements assignments
over a matrix *A* where *A_ij* is the cost of assigning asset *i* to task *j*.

### Instantiating assignment problems ###

```
[1] pry(main)> load "hungarian.rb"
=> true
[2] pry(main)> cost_matrix = Matrix[[38, 89, 33, 86, 47], [19, 27, 48, 87, 48], [32, 19, 98, 63, 77], [76, 58, 25, 22, 51], [89, 11, 44, 96, 21]]
=> Matrix[[38, 89, 33, 86, 47], [19, 27, 48, 87, 48], [32, 19, 98, 63, 77], [76, 58, 25, 22, 51], [89, 11, 44, 96, 21]]
[3] pry(main)> assignment = Assignment.new(cost_matrix)
=> #<Assignment:0x000055935acd7060 @m=Matrix[[38, 89, 33, 86, 47], [19, 27, 48, 87, 48], [32, 19, 98, 63, 77], [76, 58, 25, 22, 51], [89, 11, 44, 96, 21]], @n=5>
[4] pry(main)> 
```

### Creating random assignments ###

```
[4] pry(main)> assignment.random_assignment!
=> [[3, 0], [2, 1], [1, 2], [4, 3], [0, 4]]
[5] pry(main)> assignment
=> #<Assignment:0x000055935acd7060
 @assignments=[[3, 0], [2, 1], [1, 2], [4, 3], [0, 4]],
 @m=Matrix[[38, 89, 33, 86, 47], [19, 27, 48, 87, 48], [32, 19, 98, 63, 77], [76, 58, 25, 22, 51], [89, 11, 44, 96, 21]],
 @n=5>
[6] pry(main)> 
```

### Creating optimal assignments ###

Optimal solution describes that permutation matrix *P*
which minimizes trace(*AP*). It is an array containing
the *i* and *j* indices of the ones in *P*.


```
[6] pry(main)> assignment.hungarian
=> [[0, 2], [1, 0], [2, 1], [3, 3], [4, 4]]
[7] pry(main)> 
```

### Testing ###

```
lori@josie-Inspiron-580:/var/dev/ruby/qap$ ruby hungarian_test.rb
Run options: --seed 27021

# Running:

..

Finished in 0.395928s, 5.0514 runs/s, 255.0968 assertions/s.

2 runs, 101 assertions, 0 failures, 0 errors, 0 skips
lori@josie-Inspiron-580:/var/dev/ruby/qap$ 
```
