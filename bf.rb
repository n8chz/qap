# Brute force solution of {{very very small}} QAPs

require "matrix"

# generate n random points in the unit square [0,1]^2
def random_points(n)
  a = Array.new(n) {Vector[rand, rand]}
end

# minimize matrix
def minimaj(m)
  n = m.row_count
  r = 0...n
  c = 0...n
end

# create a random flow matrix, which is symmetric
def random_flow(n, min = 0, max = 2)
  m = Matrix.zero(n, n)
  (0...n).each do |i|
    (i+1...n).each do |j|
      m[i, j] = rand(min..max)
      m[j, i] = m[i, j]
    end
  end
  m
end

# create a symmetric distance matrix for point array of points
def dist_mat(points)
  n = points.count
  m = Matrix.zero(n, n)
  (0...n).to_a.combination(2).each do |i, j|
    d = (points[i]-points[j]).norm
    m[i, j] = d
    m[j, i] = d
  end
  m
end

# generate random QAP problem
def random_problem(n = 9)
  pts = random_points(n)
  dist = dist_mat(pts)
  flow = random_flow(n)
  [flow, dist]
end

# calculate the cost of a given ordering of the points
def cost(flow, dist, perm = (0...flow.row_count).to_a)
  n = flow.row_count
  score = 0
  (0...n).each do |i|
    (i...n).each do |j|
      score += flow[perm[i], perm[j]]*dist[i, j]
    end
  end
  score
end



# hamming distance, or number of positions not occupied by same thing
def hamming_dist(seq1, seq2)
  seq1.zip(seq2).count {|e1, e2| e1 != e2}
end

# find the global optimum given a flow matrix and distance (or cost) matrix
def brute_force(flow, dist) # seems 9 is the largest size that's practical
  n = flow.row_count
  list = (0...n).to_a.permutation.map {|p| [p, cost(flow, dist, p)]}
  list = list.sort_by {|_, cost| cost}
  table = list.group_by {|p, _| hamming_dist(list[0][0], p)}
  table = table.map do |dist, list|
    [dist, list.min_by {|_, c| c}]
  end.sort_by {|_, (_, c)| c}.each_with_object([]) do |data, a|
    a << data if a.all? {|dist, _| dist < data[0]}
  end.to_h 
  table
  # list.transpose[0]
end

# see also cdipaolo https://math.stackexchange.com/questions/2876711/maximize-the-trace-of-a-matrix-by-permuting-its-rows#comment5938370_2876711
def grid_perm(grid)
  cells = grid.each_with_index.to_a
  perm = [0]*grid.row_count
  until cells.empty?
    _, i, j = cells.min_by(&:first)
    perm[i] = j
    cells.delete_if {|_, ii, jj| i == ii || j == jj}
  end
  perm
end

def grid_all_perms(grid)
  n = grid.row_count
  (0...n).to_a.permutation.map do |p|
    [(0...n).map {|i| grid[i, p[i]]}.sum, p]
  end.sort_by(&:first).map(&:last)
end

def test_grid_perm(n = 9)
  loop do
    grid = Matrix.build(n, n) {rand}
    gap = grid_all_perms(grid)
    f = gap.count
    rank = gap.index(grid_perm(grid))
    p rank.to_f/f
  end
end

# grid[i,j] is average cost over all permutations w. jth point in ith position
def grid(flow, dist) # seems 9 is the largest size that's practical
  n = flow.row_count
  grid = Matrix.zero(n, n)
  list = (0...n).to_a.permutation.map do |p|
    c = [p, cost(flow, dist, p)]
    p.zip(0..).each do |j, i|
      grid[i, j] += c[1]
    end
    c
  end.sort_by {|_, cost| cost}
  puts "\n#{list[0][0]}"
  # puts "row: #{grid.row_vectors.map {|v| v.zip(0..).min_by(&:first)[1]}}"
  # puts "col: #{grid.column_vectors.map {|v| v.zip(0..).min_by(&:first)[1]}}\n\n"
  puts "#{grid.each_with_index.min_by(&:first)}"
  grid
end

# Access Hungarian algorithm from other file:
require_relative 'hungarian.rb'

def hungarian(m)
  Assignment.new(m).hungarian
end

# Create a grid from a random sample of permutations of (0...n).
# The _ij entry of the grid is the sum of the costs
# of those permutations which transpose the ith entry to the jth position.
def sample_grid(flow, dist, nmax = 1000000)
  n = flow.row_count
  nn = n*100
  nn = [nn, nmax].max
  grid = Matrix.zero(n, n)
  nn.times do
    p = (0...n).to_a.sample
    c = cost(flow, dist, p)
    p.zip(0..).each do |j, i|
      grid[i, j] += c
    end
  end
  hungarian(grid)
end

# like grid, but minimizing damage control instead of averages
def max_grid(flow, dist) # seems 9 is the largest size that's practical
  n = flow.row_count
  grid = Matrix.zero(n, n)
  list = (0...n).to_a.permutation.map do |p|
    c = [p, cost(flow, dist, p)]
    p.zip(0..).each do |j, i|
      grid[i, j] = c[1] if c[1] > grid[i, j]
    end
    c
  end.sort_by {|_, cost| cost}
  puts "\nopt: #{list[0][0]}"
  puts "row: #{grid.row_vectors.map {|v| v.zip(0..).min_by(&:first)[1]}}"
  puts "col: #{grid.column_vectors.map {|v| v.zip(0..).min_by(&:first)[1]}}\n\n"
  # puts "#{grid.row_vectors.map{|v| v.zip(0..).min_by(&:first)}.zip(0..).min_by{|x| x[0][0]}}"
  # puts "#{grid.round(3)}"
  nil # nominally return grid?
end

def survey(n = 9)
  loop do
    flow = random_flow(n)
    points = random_points(n)
    dist = dist_mat(points)
    # pp brute_force(flow, dist)
    max_grid(flow, dist)
  end
end

def false_bottoms(perms)
  perms.group_by {|perm| hamming_dist(perms[0], perm)}
end

def dom(perm0, (perm1, rank1), (perm2, rank2))
  hamming_dist(perm0, perm1) > hamming_dist(perm0, perm2) && rank1 < rank2
end

# solns is all permutations sorted by cost, ascending
# such as return value of brute_force
=begin
def false_bottoms(solns)
  opt = solns.shift
  n = solns.count
  solns.zip(1..).each_with_object([]) do |datum, list|
    puts "#{datum[1]} of #{n}"
    list.delete_if {|item| dom(opt, datum, item)}
    list << datum if list.none? {|item| dom(opt, item, datum)}
  end
end
=end

# draw a system as an svg
def draw(points, flow)
  svg = %Q(
    <svg version="1.1" baseProfile="full" width="300" height="300"
      viewBox="-0.1 -0.1 1.2 1.2" xmlns="http://www.w3.org/2000/svg">
    <rect x="0" y="0" height="1" width="1" fill="green" />
  )
  n = points.count
  svg << (0...n).to_a.combination(2).map do |n1, n2|
    i, j = [n1, n2].sort
    f = flow[i, j]
    next if f.zero?
    "<line x1=\"#{points[i][0]}\" x2=\"#{points[j][0]}\" y1=\"#{points[i][1]}\" y2=\"#{points[j][1]}\" stroke=\"orange\" stroke-width=\"#{f*0.005}\" />"
  end.join("\n")
  svg << points.zip(0..).map do |point, index|
    x, y = point.to_a
    node = "<circle cx=\"#{x}\" cy=\"#{y}\" r=\"0.05\" fill=\"yellow\" />\n"
    node << "<text x=\"#{x}\" y=\"#{y}\" font-size=\"0.07px\""
    node << " dominant-baseline=\"middle\" text-anchor=\"middle\">#{index}</text>\n"
    node
  end.join
  svg << %Q(
    </svg>
  )
end

# apply a permutation of (0...n).to_a  to an array of n points
def permute(points, perm)
  Array.new(points.count) {|i| points[perm[i]]}
end

def time_trial(n = 9)
  f = random_flow(n)
  p = random_points(n)
  d = dist_mat(p)
  brute_force(f, d).take(15)
end

# find a global optimum by brute force, and draw a picture of it
def draw_good(n = 9)
  f = random_flow(n)
  p = random_points(n)
  d = dist_mat(p)
  perm = brute_force(f, d)[0]
  draw(permute(p, perm), f)
end

# see how large a system we have time for
def averages
  (0..).each do |n|
    puts "#{n}, #{time_trial(n)}"
  end
end

