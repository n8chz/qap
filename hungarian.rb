require "matrix"

class Assignment

  attr_reader :cost, :tableau

  def initialize(costs)
    @n = costs.length
    raise DomainError unless costs.map(&:length).uniq == [@n]
    @all_rows = (0...@n).to_a
    @cost = costs
    @tableau = costs.map(&:clone)
  end

  def self.random(n)
    costs = Array.new(n) {Array.new(n) {rand(10..99)}}
    self.new(costs)
  end

  def solve
    make_first_zeros
    loop do
      lines = cover_zeros
      if lines[0].count+lines[1].count >= @n
        t = @tableau.transpose
        assignment = (0...@n).map {|i| [i, []]}.to_h
        (0...@n).each do |i|
          (0...@n).each do |j|
            assignment[i] << j if @tableau[i][j].zero?
          end
        end
        loop do
          assignment.each do |k, v|
            if v.length == 1
              assignment.each do |k1, v1|
                next if k == k1
                assignment[k1].delete(v.first)
              end
            end
          end
          return assignment if assignment.values.map(&:length).uniq == [1]
          unresolved = assignment.values.select {|v| v.length > 1}
          return assignment if unresolved.flatten.uniq.length == unresolved.length
        end
      end
      make_zeros(lines)
    end
  end

  def cover_zeros
    # puts @tableau.display
    row_masks = @tableau.map do |row|
      row.map {|n| n.zero? ? 0 : 1}
    end
    combos = @all_rows.zip(row_masks).map {|i, m| [[i], m]}
    loop do
      new_combos = []
      return [@all_rows, []] if combos.empty?
      height = combos[0][0].count+1
      combos.each do |rows, mask|
        (rows.max+1...@n).each do |new_row|
          new_mask = row_masks[new_row].zip(mask).map {|m, n| m*n}
          width = new_mask.count {|n| n != 0}
          next if width < 2
          if height+width > @n
            new_cols = (0...@n).select {|j| new_mask[j].zero?}
            return [@all_rows-rows-[new_row], new_cols]
          else
            new_combos << [rows+[new_row], new_mask]
          end
        end
      end
      combos = new_combos
    end
  end

  def assignments
    puts "assignments"
  end

  def make_first_zeros
    make_zeros_in_rows
    @tableau = @tableau.transpose
    make_zeros_in_rows
    @tableau = @tableau.transpose
  end

  def make_zeros(lines)
    covered_rows, covered_cols = lines
    uncovered_rows = @all_rows-covered_rows
    uncovered_cols = @all_rows-covered_cols
    uncovered_cells = uncovered_rows.product(uncovered_cols).map do |i, j|
      [i, j, @tableau[i][j]]
    end
    k = uncovered_cells.transpose.last.min
    uncovered_cells.each do |i, j, n|
      @tableau[i][j] -= k
    end
    covered_rows.product(covered_cols).each do |i, j|
      @tableau[i][j] += k
    end
  end

  private

  def make_zeros_in_rows
    @tableau.map! do |row|
      smallest = row.min
      row.map! {|a| a-smallest}
    end
  end
end

class Matrix
  def block(row_range, col_range)
    rr = row_range.to_a
    cr = col_range.to_a
    Matrix.build(rr.count, cr.count) do |i, j|
      self[rr[i], cr[j]]
    end
  end

  def display
    cols = Array.new(column_count) do |j|
      Array.new(row_count) do |i|
        # e = element(i, j).to_f.round(4)
        e = element(i, j)
        if e.denominator == 1
          e.numerator.to_s
        else
          e.to_s
        end
      end
    end
    col_w = cols.map {|col| col.max_by(&:length).length}
    cols.transpose.map do |row|
      row.zip(col_w).map {|cell, width| cell.rjust(width)}.join(" ")
    end.map {|row| row+"\n"}.join+"-----------\n"
  end
end

class Array
  def display
    widths = transpose.map {|c| c.map {|k| ('%d'%k).length}.max}
    map do |row|
      row.zip(widths).map do |number, width|
        number.to_s.rjust(width)
      end.join(" ")
    end.join("\n")+"\n\n"
  end

  def binarize
    ([0]+self).reduce do |b, d|
      raise DomainError unless d.kind_of?(Numeric)
      2*b + (d.zero? ? 0 : 1)
    end
  end
end

class Integer
  def digit_sum(base=2)
    value = 0
    n = self
    until n.zero?
      n, r = n.divmod(base)
      value += r
    end
    value
  end

  # TODO; use rjust instead of %b, fill with '1's instead of '0's
  # TODO: rewrite to do arithmetically, instead of from string
  def zero_columns(n)
    ('%b'%self).rjust(n, '0').chars.zip(0...n).filter_map do |bit, line|
      bit == '0' ? line : nil
    end
  end
end

module Enumerable
  def binarize
    ([0]+to_a).reduce do |b, d|
      raise DomainError unless d.kind_of?(Numeric)
      2*b + (d.zero? ? 0 : 1)
    end
  end
end


def survey(n=6)
  count = 0
  loop do
    a = Assignment.random(n).solve
    if a
      puts "A pathological case found after #{count} iterations:"
      puts a.tableau.display
      return true
    end
    count += 1
  end
end
