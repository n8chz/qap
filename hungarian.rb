require "matrix"

class Assignment
  def initialize(m)
    @n = m.row_count
    raise "not a square matrix" unless m.column_count == @n
    @m = m.clone
  end

  def hungarian
    @assignments = []
    build_tableau!
    while @n > 0
      base_rows!
      base_columns!
      while assign_from_rows! || assign_from_columns!
      end
    end
    @assignments
  end

  def cost
    hungarian unless @assignments
    @assignments.map {|i, j| @m[i, j]}.sum
  end

  def random_assignment!
    @assignments = (0...@n).to_a.shuffle.zip(0..)
  end

  private

  def build_tableau!
    @tableau = Matrix.build(@n+2, @n+2) do |i, j|
      if i == 0
        j-1
      elsif j == 0
        i-1
      elsif i == @n+1 || j == @n+1
        0
      else
        @m[i-1, j-1]
      end
    end
  end

  def base_rows!
    (1..@n).each do |i|
      offset = @tableau.row(i).to_a.slice(1, @n).min
      (1..@n).each do |j|
        @tableau[i, j] -= offset
      end
    end
  end

  def base_columns!
    (1..@n).each do |j|
      offset = @tableau.column(j).to_a.slice(1, @n).min
      (1..@n).each do |i|
        @tableau[i, j] -= offset
      end
    end
    count_zeros!
  end

  def count_zeros!
    (1..@n).each do |i|
      @tableau[i, @n+1] = @tableau.row(i).to_a.slice(1, @n).count(0)
    end
    (1..@n).each do |j|
      @tableau[@n+1, j] = @tableau.row(j).to_a.slice(1, @n).count(0)
    end
  end

  def assign_from_rows!
    i = @tableau.column(@n+1).to_a.slice(1, @n).index(1)
    if i
      i += 1
      j = @tableau.row(i).to_a.slice(1, @n).index(0)+1
      @assignments << [@tableau[i, 0], @tableau[0, j]]
      @tableau = @tableau.first_minor(i, j)
      @n -= 1
      count_zeros!
      return i
    end
    return nil
  end

  def assign_from_columns!
    j = @tableau.row(@n+1).to_a.slice(1, @n).index(1)
    if j
      j += 1
      i = @tableau.column(j).to_a.slice(1, @n).index(0)+1
      @assignments << [@tableau[i, 0], @tableau[0, j]]
      @tableau = @tableau.first_minor(i, j)
      @n -= 1
      count_zeros!
      return j
    end
    return nil
  end
end
