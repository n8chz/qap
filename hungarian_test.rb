require 'minitest/autorun'
require_relative 'hungarian'

SIZE = 7

# Common test data version: 1.0.0 9f3d48a
class HungarianTest < Minitest::Test
  def test_exhaustive
    100.times do
      a = Assignment.random(SIZE)
      t1 = Time.new.to_f
      expected = (0...SIZE).to_a.permutation.map do |p|
        (0...SIZE).map {|i| a.cost[i][p[i]]}.sum
      end.min
      t2 = Time.new.to_f
      t3 = Time.new.to_f
      solution = a.solve
      t4 = Time.new.to_f
      next if solution.values.any? {|v| v.length > 1}
      actual = solution.map {|i, j| a.cost[i][j[0]]}.sum
      assert_equal(expected, actual)
      assert_operator(t4-t3, :<=, t2-t1)
    end
  end
end
