require 'minitest/autorun'
require_relative 'hungarian'

# Common test data version: 1.0.0 9f3d48a
class HungarianTest < Minitest::Test
  def test_for_optimality
    a = Matrix.build(100, 100) {rand(11..99)}
    s = Assignment.new(a)
    b = s.hungarian
    c = s.cost
    100.times do
      s.random_assignment!
      k = s.cost
      assert(k <= c)
    end
  end

  def test_for_uniqueness
    a = Matrix.build(100, 100) {rand(11..99)}
    r = (0...100).to_a
    expected = [r, r]
    actual = Assignment.new(a).hungarian.transpose.map(&:sort)
    assert_equal expected, actual
  end
end
