library(purrr)
library(pracma)
library(semTools)
library(prodlim)

random_locations <- function(n) {matrix(runif(2*n, 0, 1), nrow = 2)}

distances <- function(locations) {
  # locations <- random_locations(n)
  n = ncol(locations)
  m <- matrix(rep(0, n*n), nrow = n)
  for (i in 1:(n-1)) {
    for (j in (i+1):n) {
      m[i, j] <- Norm(locations[, i]-locations[, j])
      m[j, i] <- m[i, j]
    }
  }
  return(m)
}

random_flow <- function(n) {
  flow <- matrix(rep(0, n*n), nrow = n)
  for (i in 1:(n-1)) {
    for (j in (i+1):n) {
      flow[i, j] <- rdunif(1, -2, 2)
      flow[j, i] <- flow[i, j]
    }
  }
  return(flow);
}

cost <- function(flow, dist, perm=NULL) {
  if (length(perm) == 0) {
    permutation <- 1:nrow(flow)
  }
  else {
    permutation <- perm
  }
  permuted_flow <- perm_rows_cols(flow, permutation)
  return(sum(permuted_flow*dist));
}

# h/t Museful https://stackoverflow.com/a/20199902/948073
permutations <- function(n){
    if(n==1){
        return(matrix(1))
    } else {
        sp <- permutations(n-1)
        p <- nrow(sp)
        A <- matrix(nrow=n*p,ncol=n)
        for(i in 1:n){
            A[(i-1)*p+1:p,] <- cbind(i,sp+(sp>=i))
        }
        return(A)
    }
}

perm_rows_cols <- function(m, p) {
  n <- dim(m)[1]
  result <- matrix(rep(0, length(m)), nrow = n)
  for (i in 1:(n-1)) {
    for (j in (i+1):n) {
      result[i, j] <- m[p[i], p[j]]
      result[j, i] <- result[i, j]
    }
  }
  return(result)
}

run_test <- function(n) {
  loc <- random_locations(n)
  d <- distances(loc)
  flow <- random_flow(n)
  perms <- permutations(n)
  # h/t lionel-
  # https://github.com/tidyverse/purrr/issues/341#issuecomment-353959893
  return(unlist(map(array_tree(perms, 1), function(p) {
    # cost(perm_rows_cols(flow, p), d)
    cost(flow, d, p)
  })))
}

random_problem <- function(n) {
  problem <- list()
  problem$locations <- random_locations(n)
  problem$distances <- distances(problem$locations)
  problem$flow <- random_flow(n)
  return(problem)
}

global_optimum <- function(problem) {
  perms <- permutations(ncol(problem$locations))
  costs <- vector(length = nrow(perms))
  min_cost <- Inf
  global_min <- c()
  # h/t Dirk Eddelbuettel https://stackoverflow.com/a/4236383/948073
  for (i in 1:nrow(perms)) {
    # c = cost(perm_rows_cols(problem$flow, perms[i,]), problem$distances)
    c = cost(problem$flow, problem$distances, perms[i,])
    costs[i] <- c
    if (c < min_cost) {
      min_cost <- c
      global_min <- perms[i,]
      global_min_index <- i
    }
  }
  value = list()
  value$permutations = perms
  value$costs = costs
  value$global_min = global_min
  value$global_min_index = global_min_index
  return(value)
}

transpositions <- function(permutation) {
  n <- length(permutation)
  # value <- matrix(permutation, nrow=1)
  value <- c()
  for (i in 1:(n-1)) {
    for (j in (i+1):n) {
      new_permutation <- permutation
      new_permutation[i] <- permutation[j]
      new_permutation[j] <- permutation[i]
      value <- rbind(value, new_permutation, deparse.level=0)
    }
  }
  return(value)
}

watershed <- function(confluence, go, stash=c()) {
  cost <- go$costs[confluence]
  t <- transpositions(go$permutations[confluence, ])
  for (i in 1:nrow(t)) {
    p <- row.match(t[i, ], go$permutations)
    if (is.na(match(p, stash))) { # why this not work?
      tcost <- go$costs[p]
      if (tcost > cost) {
        stash <- unique(c(stash, p, watershed(p, go, stash))) # unique is kludge
      }
    }
  }
  return(stash)
}

one_watershed_count <- function(n) {
  p <- random_problem(n)
  g <- global_optimum(p)
  return(length(watershed(g$global_min_index, g)))
}

hist_of_watersheds <- function(size, iter) {
  print(Sys.time())
  w <- NULL
  for (i in 1:iter) {
    print(i)
    w <- c(w, one_watershed_count(size))
  }
  print(Sys.time())
}

experiment <- function() {
  f <- data.frame()
  for (i in 1:20) {
    costs <- run_test(9)
    f <- rbind(
      f,
      data.frame(
        mean = mean(costs),
        sd = sd(costs),
        skew = skew(costs, population=TRUE),
        kurtosis = kurtosis(costs, population=TRUE)
      )
    )
  }
  return(f)
}
