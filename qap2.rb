# Brute force solution of {{very very small}} QAPs

# TODO: Explore some of these questions:
# 1. Is there a lower bound for r/p, where
#    r is the number of monotonically reachable
#    permutations and p=n!
# 2. Does the probability of a layout having all
#    permutations reachable vary with n?

# Access Hungarian algorithm from other file:
require_relative 'hungarian.rb'

def hungarian(m)
  Assignment.new(m).hungarian.sort_by {|i, _| i}.map {|_, j| j}
end

require "matrix"

# generate n random points in the unit square [0,1]^2
def random_points(n)
  a = Array.new(n) {Vector[rand, rand]}
end

# create a random flow matrix, which is symmetric
def random_flow(n, min = 0, max = 2)
  m = Matrix.zero(n, n)
  (1...n).each do |i|
    row = [0]*(i+1)
    # ensure connectedness:
    while row.all? {|i| i.zero?}
      row.map! {rand(min..max)}
      # puts "#{row}"
    end
    row.each do |j|
      m[i, j] = m[j, i] = rand(1..2)
    end
  end
  m
end

# create a symmetric distance matrix for point array of points
def dist_mat(points)
  n = points.count
  m = Matrix.zero(n, n)
  (0...n).to_a.combination(2).each do |i, j|
    d = (points[i]-points[j]).norm
    m[i, j] = d
    m[j, i] = d
  end
  m
end

# generate random QAP problem
def random_problem(n = 9)
  pts = random_points(n)
  dist = dist_mat(pts)
  flow = random_flow(n)
  [pts, flow, dist]
end

# Calculate the cost of a given ordering of the points.
# Default permutation is (0...n).
def cost(flow, dist, perm = (0...flow.row_count).to_a)
  n = flow.row_count
  score = 0
  (0...n).each do |i|
    (i...n).each do |j|
      score += flow[perm[i], perm[j]]*dist[i, j]
    end
  end
  score
end

# entire solution space for a small problem
def all_perms(flow, dist)
  n = flow.row_count
  (0...n).to_a.permutation.sort_by {|p| cost(flow, dist, p)}
end

# grid[i,j] is average cost over all permutations w. jth point in ith position
def grid(flow, dist) # seems 9 is the largest size that's practical
  n = flow.row_count
  grid = Matrix.zero(n, n)
  list = (0...n).to_a.permutation.map do |p|
    c = [p, cost(flow, dist, p)]
    p.zip(0..).each do |j, i|
      grid[i, j] += c[1]
    end
    c
  end.sort_by {|_, cost| cost}
  grid
end

# Create a grid from a random sample of permutations of (0...n).
# The _ij entry of the grid is the sum of the costs
# of those permutations which transpose the ith entry to the jth position.
def sample_grid(flow, dist, nmax = 1000000)
  n = flow.row_count
  nn = n*100
  nn = [nn, nmax].max
  grid = Matrix.zero(n, n)
  nn.times do
    p = (0...n).to_a.sample
    c = cost(flow, dist, p)
    p.zip(0..).each do |j, i|
      grid[i, j] += c
    end
  end
  hungarian(grid)
end


def survey(n = 9)
  loop do
    _, flow, dist = random_problem(n)
    a = all_perms(flow, dist)
    h = dipaolo(grid(flow, dist))
    puts "#{a.index(h).to_f/a.count}"
  end
end

# Ranks (by cost) of ij mappings in global optimum, among sums in grid matrix.
# Lowest has rank 0, highest has rank n^2-1.
def experiment_1(n = 9)
  loop do
    _, flow, dist = random_problem(n)
    g = grid(flow, dist)
    opt = all_perms(flow, dist).first
    ijr = g.each_with_index.to_a.sort_by do |c, _, _|
      c
    end.zip(0..).to_a.map do |(_, i, j), k|
      [i, j, k]
    end 
    p ijr.select {|i, j, _| opt[i] == j}.map {|_, _, k| k}
  end
end

# Explore relationships between <some distance metric> (from global optimum) and cost.
def experiment_2(n=9)
  pts, flow, dist = random_problem(n)
  pc = (0...n).to_a.permutation.map {|p| [p, cost(flow, dist, p)]}
  go, _ = pc.min_by {|_, c| c}
  pc.map {|p, c| [hamming(pts, p, go), c]}
end

# see also cdipaolo
# https://math.stackexchange.com/questions/2876711/maximize-the-trace-of-a-matrix-by-permuting-its-rows#comment5938370_2876711
def dipaolo(grid)
  cells = grid.each_with_index.to_a
  perm = [0]*grid.row_count
  until cells.empty?
    _, i, j = cells.min_by(&:first)
    perm[i] = j
    cells.delete_if {|_, ii, jj| i == ii || j == jj}
  end
  perm
end

# Sum of combined straight line movement of points to transform p1 into p2.
def permdist(points, p1, p2)
  p1.zip(p2).map do |i, j|
    (points[i]-points[j]).norm
  end.sum # TODO: calculate Pearson correlation coefficient
end

def hamming(_, p1, p2)
  p1.zip(p2).count {|a, b| a != b}
end

def transpositions(points, flow, dist)
  n = points.count
  pc = (0...n).to_a.permutation.map {|p| [p, cost(flow, dist, p)]}
  go, _ = pc.min_by {|_, c| c}
  trans = {go => []}
  ct = 0
  loop do
    again = false
    trans.keys.each do |p|
      (0...n).to_a.combination(2).each do |i, j|
        t = p.clone
        t[i] = p[j]
        t[j] = p[i]
        # nb this may miss some destinations
        if cost(flow, dist, t) > cost(flow, dist, p)
          unless trans.keys.include?(t)
            trans[t] = trans[p]+[[i, j]]
            again = true
            # puts "#{ct}: #{trans[t]}"
            ct += 1
          end
        end
      end
    end
    return [go, trans] unless again
  end
end

def reachable(points, flow, dist)
  n = points.count
  pc = (0...n).to_a.permutation.map {|p| [p, cost(flow, dist, p)]}
  go, _ = pc.min_by {|_, c| c}
  perms = [go]
  ct = 0
  lc = 0
  loop do
    lc += 1
    puts "iteration #{lc}"
    again = false
    perms.each do |p|
      (0...n).to_a.combination(2).each do |i, j|
        t = p.clone
        t[i] = p[j]
        t[j] = p[i]
        # nb this may miss some destinations
        if cost(flow, dist, t) > cost(flow, dist, p)
          unless perms.include?(t)
            perms << t
            again = true
            # puts "#{ct}: #{trans[t]}"
            ct += 1
          end
        end
      end
    end
    return perms unless again
  end
end

# experiment #6 is simply correlation between transposition distance and cost
def experiment_6(n = 6)
  points, flow, dist = random_problem(n)
  pc = (0...n).to_a.permutation.to_a.map do |p|
    [p, cost(flow, dist, p)]
  end.to_h
  bestp, bestc = pc.min_by {|p, c| c}
  perms = [[bestp, bestc, 0]]
  perms = {bestp => [bestc, 0]}
  ct = 0
  loop do
    ct += 1
    again = false
    perms.keys.each do |p|
      d = perms[p][1]
      (0...n).to_a.combination(2).each do |i, j|
        t = p.clone
        t[i] = p[j]
        t[j] = p[i]
        unless perms.keys.include?(t)
          # puts "#{perms.count}"
          perms[t] = [pc[t], d+1]
          again = true
        end
      end
    end
    unless again
      return perms.values.corr
    end
  end
end

# draw a system as an svg
def draw(points, flow)
  svg = %Q(
    <svg version="1.1" baseProfile="full" width="300" height="300"
      viewBox="-0.1 -0.1 1.2 1.2" xmlns="http://www.w3.org/2000/svg">
    <rect x="0" y="0" height="1" width="1" fill="green" />
  )
  n = points.count
  svg << (0...n).to_a.combination(2).map do |n1, n2|
    i, j = [n1, n2].sort
    f = flow[i, j]
    next if f.zero?
    "<line x1=\"#{points[i][0]}\" x2=\"#{points[j][0]}\" y1=\"#{points[i][1]}\" y2=\"#{points[j][1]}\" stroke=\"orange\" stroke-width=\"#{f*0.005}\" />"
  end.join("\n")
  svg << points.zip(0..).map do |point, index|
    x, y = point.to_a
    node = "<circle cx=\"#{x}\" cy=\"#{y}\" r=\"0.05\" fill=\"yellow\" />\n"
    node << "<text x=\"#{x}\" y=\"#{y}\" font-size=\"0.07px\""
    node << " dominant-baseline=\"middle\" text-anchor=\"middle\">#{index}</text>\n"
    node
  end.join
  svg << %Q(
    </svg>
  )
end

# apply a permutation of (0...n).to_a  to an array of n points
def permute(points, perm)
  Array.new(points.count) {|i| points[perm[i]]}
end


# drawing pictures of the most pathological layouts,
# in terms of monotonic reachability of arrangements
def experiment_7(n = 6)
  reachable = Float::INFINITY
  gallery = []
  loop do
    pts, flow, dist = random_problem(n)
    go, trans = transpositions(pts, flow, dist)
    if trans.count < reachable
      reachable = trans.count
      p reachable
      # gallery << [draw(permute(pts, go), flow)]
      File.open("#{reachable}.svg", File::RDWR|File::CREAT, 0644) do |f|
        f.write(draw(permute(pts, go), flow))
      end
    end
  end
end

def perms_costs(pts, flow, dist)
  n = pts.count
  perms = (0...n).to_a.permutation
  costs = perms.map {|perm| cost(flow, dist, perm)}
  perms.zip(costs).to_h
end

def pairwise(perm)
  perm.combination(2).map do |i, j|
    variant = perm.clone
    variant[i] = perm[j]
    variant[j] = perm[i]
    variant
  end
end

def watershed(perm, perm_cost, stash=[])
  cost1 = perm_cost[perm]
  pairwise(perm).each do |p|
    unless stash.include?(p)
      if perm_cost[p] > cost1
        stash << p
        stash.uniq!
        stash |= watershed(p, perm_cost, stash)
      end
    end
  end
  # puts "#{stash.count} permutations in stash"
  stash
end

# return size of watershed of one random problem
def count_one_watershed(n)
  pts, flow, dist = random_problem(n)
  pc = perms_costs(pts, flow, dist)
  go, _ = pc.min_by {|_, c| c}
  w = watershed(go, pc)
  w.count
end

def watershed_data(n = 6, running_time = 3600)
  start = Time.new.to_f
  data = []
  while Time.new.to_f < start+running_time
    data << count_one_watershed(n)
  end
  data
end

def factorial(n)
  (2..n).reduce(&:*)
end

# consider layouts where monotonic reachability spans all permutations
def experiment_9(n = 6)
  good = 0
  all = 0
  loop do
    all += 1
    pts, flow, dist = random_problem(n)
    go, trans = transpositions(pts, flow, dist)
    puts "#{trans.count}"
    if trans.count == factorial(n)
      good += 1
      puts "found one!"
      return
    end
  end
end

def experiment_5(n = 7)
  pts, flow, dist = random_problem(n)
  perms = (0...n).to_a.permutation.to_a-reachable(pts, flow, dist)
  grid = Matrix.build(n, n) do |i, j|
    perms.count {|p| p[i] == j}
  end
  pp grid.to_a
  pp [grid.row_vectors.map(&:sum), grid.column_vectors.map(&:sum)]
  grid
end

def experiment_3(n = 7)
  pts, flow, dist = random_problem(n)
  transpositions(pts, flow, dist)
  # puts "#{trans.count} nodes in resulting graph"
end

def experiment_4()
  loop do
    trans = experiment_3(6)
    puts "#{trans.length/7.2}%"
  end
end

class Array
  def corr
    n, sx, sy, sxy, sx2, sy2 = [0, 0, 0, 0, 0, 0]
    each do |x, y|
      n += 1
      sx += x
      sy += y
      sxy += x*y
      sx2 += x*x
      sy2 += y*y
    end
    (n*sxy-sx*sy)/Math.sqrt((n*sx2-sx*sx)*(n*sy2-sy*sy))
  end
end


