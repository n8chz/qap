require "matrix"

# generate random QAP problem
def random_problem(n = 9)
  pts = random_points(n)
  dist = dist_mat(pts)
  flow = random_flow(n)
  [pts, flow, dist]
end

# generate n random points in the unit square [0,1]^2
def random_points(n)
  a = Array.new(n) {Vector[rand, rand]}
end

# create a random flow matrix, which is symmetric
def random_flow(n, min = 0, max = 2)
  m = Matrix.zero(n, n)
  (1...n).each do |i|
    row = [0]*(i+1)
    # ensure connectedness:
    while row.all? {|i| i.zero?}
      row.map! {rand(min..max)}
      # puts "#{row}"
    end
    row.each do |j|
      m[i, j] = m[j, i] = rand(1..2)
    end
  end
  m
end

# create a symmetric distance matrix for point array of points
def dist_mat(points)
  n = points.count
  m = Matrix.zero(n, n)
  (0...n).to_a.combination(2).each do |i, j|
    d = (points[i]-points[j]).norm
    m[i, j] = d
    m[j, i] = d
  end
  m
end

# Calculate the cost of a given ordering of the points.
# Default permutation is (0...n).
def cost(flow, dist, perm = (0...flow.row_count).to_a)
  n = flow.row_count
  score = 0
  (0...n).each do |i|
    (i...n).each do |j|
      score += flow[perm[i], perm[j]]*dist[i, j]
    end
  end
  score
end

def transpositions(perm)
  n = perm.count
  (0...n).to_a.combination(2).map do |i, j|
    p2 = perm.clone
    p2[i] = perm[j]
    p2[j] = perm[i]
    p2
  end
end

def descent_graph(pts, dist, flow)
  n = pts.count
  perms = (0...n).to_a.permutation.to_a
  costs = perms.map {|p| cost(flow, dist, p)}
  pc = perms.zip(costs).to_h
  g = Matrix.zero(perms.count, perms.count)
  # TODO: Fill in graph G with a directed edge for each case where
  # 1. permutations differ by one transposition
  # 2. travel in direction of edge decreases cost.
  perms.each_with_index do |p, i|
    transpositions(p).each do |t|
      if pc[t] > pc[p]
        j = perms.index(t)
        g[j, i] = 1
      end
    end
  end
  g
end
