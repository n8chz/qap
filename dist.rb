require "matrix"

class Matrix
  def self.random_flow(n)
    m = Matrix.zero(n, n)
    (0...n).each do |i|
      (i+1...n).each do |j|
        m[i, j] = rand(-2..2)
        m[j, i] = m[i, j]
      end
    end
    m
  end

  def self.random_dist(n)
    points = Array.new(n) {Vector[rand, rand]}
    m = Matrix.zero(n, n)
    (0...n).to_a.combination(2).each do |i, j|
      d = (points[i]-points[j]).norm
      m[i, j] = d
      m[j, i] = d
    end
    m
  end
end

def array_avg(a)
  a.sum/a.count
end

# dist as in distribution
def dist(flow, dist) # seems 9 is the largest size that's practical
  m = flow.row_count
  start = Time.new.to_f
  scores = (0...m).to_a.permutation.map do |p|
    score = 0
    (0...m).each do |i|
      (i...m).each do |j|
        score += flow[p[i], p[j]]*dist[i, j]
      end
    end
    score
  end.sort
  q = (1..scores.count).map {|i| (2.0*i-1)/2.0/scores.count}
  frame = scores.zip(q).sort_by {|score, _| score}.keys
  icdf = frame.map {|blob| [blob[0][0], array_avg(blob.transpose.pop)]}
  finish = Time.new.to_f
  pp icdf
  # puts "#{"%.3f" % (finish-start)} seconds"
  # finish-start
end

def test
  f = Matrix.random_flow(9)
  d = Matrix.random_dist(9)
  dist(f, d)
end
